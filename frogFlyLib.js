var tFrog
function Frog(){
    tFrog = new Sprite(scene, "frog.png", 80, 80);
    tFrog.maxSpeed =10;
    tFrog.minSpeed =-3;
    tFrog.setSpeed(0);
    tFrog.setAngle(0);
    tFrog.setPosition(300,300)

    tFrog.controlFrog = function(){
         //handle events here
         if (keysDown[K_UP]) {
            if (this.speed < this.maxSpeed) {
                this.changeSpeedBy(1);
            }
        }
        if (keysDown[K_LEFT]) {
            this.changeAngleBy(-5);
        }
        if (keysDown[K_RIGHT]) {
            this.changeAngleBy(5);
        }
        if (keysDown[K_DOWN]) {
            if (this.speed > this.minSpeed) {
                this.changeSpeedBy(-1);
            }
        }
    }
    return tFrog
}
function Fly(){
    tFly = new Sprite(scene, "fly.png", 30, 30);
    tFly.setSpeed(7);
    tFly.setAngle(0);

    tFly.wriggle = function(){
         newDir = (Math.random()*90)-45;
         this.changeAngleBy(newDir);
    }
    return tFly
}